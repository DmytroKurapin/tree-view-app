import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { FetchService } from '../../services/fetch.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FilesJsonObject } from '../../helpers/files-json-object';

@Component({
  selector: 'app-edit-node-modal',
  styleUrls: ['./edit-node-modal.component.sass'],
  template: `
    <div
      id="myModal"
      class="modal"
      [style.display]="(isVisible | async) ? 'block' : 'none'"
      (click)="onBackdropClick($event)">
      <form (ngSubmit)="close.emit(form.value)" [formGroup]="form">
        <div class="modal-content">
          <div class="modal-header">
            <span class="close" (click)="closeModal($event)">&times;</span>
            <h2>Add New File</h2>
          </div>
          <div class="modal-body">
            <p>Place To Add</p>
            <input type="radio" name="place" formControlName="place" value="folder"> Before<br>
            <input type="radio" name="place" formControlName="place" value="file"> Inside<br>
            <input type="radio" name="place" formControlName="place" value="file"> After<br>
            <p>File Type</p>
            <input type="radio" name="type" formControlName="type" value="folder" checked> Folder<br>
            <input type="radio" name="type" formControlName="type" value="file"> File<br>
            <p>Name</p>
            <input type="text" formControlName="name">
            <p>File Extension</p>
            <select formControlName="ext">
              <option value=""></option>
              <option *ngFor="let ext of fetchService.availableExts" value="ext">{{ext}}</option>
            </select>
          </div>
          <div class="modal-footer">
            <input type="submit" [disabled]="!form.valid">
          </div>
        </div>
      </form>
    </div>
  `
})
export class EditNodeModalComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('is-visible') isVisible: Subject<boolean>;
  @Output() close: EventEmitter<FilesJsonObject | null> = new EventEmitter();
  form: FormGroup;

  constructor(private fetchService: FetchService, private fb: FormBuilder) {
    // todo set placement 'inside' disable if selected node is file
  }

  ngOnInit() {
    this.form = this.fb.group({
      place: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      ext: ['', Validators.compose([Validators.required])],
    });
  }

  closeModal(ev) {
    ev.stopPropagation();
    this.close.emit(null);
  }

  onBackdropClick(ev) {
    if (ev.target === document.getElementById('myModal')) {
      this.close.emit(null);
    }
  }
}
