import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditNodeModalComponent } from './edit-node-modal/edit-node-modal.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [EditNodeModalComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [EditNodeModalComponent],
})
export class ComponentsModule {
}
