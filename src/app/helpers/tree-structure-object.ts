export interface TreeStructureObject {
  nodeId: string;
  nodeText: string;
  nodeIcon: string;
  nodeChild?: TreeStructureObject[];
}
