export interface FilesJsonObject {
  type: 'folder' | 'file';
  name: string;
  children?: [];
  ext?: string;
  place?: 'before' | 'after' | 'inside';
}
