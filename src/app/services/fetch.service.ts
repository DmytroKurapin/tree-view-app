import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TreeStructureObject } from '../helpers/tree-structure-object';
import { FilesJsonObject } from '../helpers/files-json-object';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FetchService {
  private lastNodeIdx = 0;
  public readonly availableExts: string[] = ['css', 'js', 'exe', 'docx', 'xlsx', 'pdf', 'jpg', 'zip', 'mp3', 'mp4', 'webm', 'mkv', 'html', 'ppt'];

  constructor(private http: HttpClient) {
  }

  getInitFilesList(): Observable<TreeStructureObject[]> {
    return this.http.get<FilesJsonObject[]>('/assets/files.json')
      .pipe(
        take(1),
        map(data => this.convertToTreeStructure(data)),
      );
  }

  public convertToTreeStructure(arr: FilesJsonObject[]): TreeStructureObject[] {
    if (arr.length === 0) {
      return [];
    }
    return arr.reduce((resArr, currObj) => {
      this.lastNodeIdx++;
      const objToAdd: TreeStructureObject = { nodeId: `${this.lastNodeIdx}`, nodeText: currObj.name, nodeIcon: '' };
      if (currObj.type === 'folder') {
        objToAdd.nodeIcon = 'folder';
        if (currObj.children) {
          objToAdd.nodeChild = this.convertToTreeStructure([...currObj.children]);
        }
      } else {
        objToAdd.nodeIcon = this.availableExts.includes(currObj.ext) ? currObj.ext : 'unknown';
      }
      resArr.push(objToAdd);
      return resArr;
    }, []);
  }
}
