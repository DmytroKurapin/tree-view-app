import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeStructureObject } from '../../helpers/tree-structure-object';
import { FetchService } from '../../services/fetch.service';
import { Subject } from 'rxjs';
import { TreeViewComponent } from '@syncfusion/ej2-ng-navigations';
import { FilesJsonObject } from '../../helpers/files-json-object';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass'],
})
export class MainComponent implements OnInit {
  public treeData: TreeStructureObject[] = [];
  public treeFields = {
    dataSource: this.treeData,
    id: 'nodeId',
    text: 'nodeText',
    child: 'nodeChild',
    iconCss: 'nodeIcon'
  };
  public isModalShown: Subject<boolean>;
  @ViewChild ('myTree') myTree: TreeViewComponent;
  private targetNodeId: string | null = null;

  constructor(private fetchService: FetchService) {
    window.onbeforeunload = this.saveCurrState.bind(this);
  }

  ngOnInit() {
    this.isModalShown = new Subject();
    const savedFiles = localStorage.getItem('files');
    if (savedFiles) {
      try {
        const parsedJson = JSON.parse(savedFiles);
        if (Array.isArray(parsedJson)) {
          return this.treeData.push(...parsedJson);
        }
      } catch (e) {
        this.fetchService.getInitFilesList().subscribe(a => this.treeData.push(...a));
      }
      return;
    }
    this.fetchService.getInitFilesList().subscribe(a => this.treeData.push(...a));
  }

  private saveCurrState() {
    localStorage.setItem('files', JSON.stringify(this.treeFields.dataSource));
  }

  public onClick(clickedData: { event: MouseEvent, node: HTMLLIElement }) {
    this.targetNodeId = this.myTree.selectedNodes[0];
    const { event } = clickedData;
    event.preventDefault();
    if (event.button === 2) {
      this.isModalShown.next(true);
    }
  }

  closeModal(obj: FilesJsonObject | null) {
    this.isModalShown.next(false);
    if (!obj) {
      this.targetNodeId = null;
      return;
    }
    // todo chec  k place prop in obj and put folder/file in appropriate one
    // todo fix it next time
    this.myTree.addNodes(this.fetchService.convertToTreeStructure([obj]), this.targetNodeId, null);
    this.targetNodeId = null;
  }
}
