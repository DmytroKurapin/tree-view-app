import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { TreeViewComponent } from '@syncfusion/ej2-ng-navigations';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  declarations: [MainComponent, TreeViewComponent],
  imports: [
    CommonModule,
    ComponentsModule
  ],
  exports: [MainComponent]
})
export class PagesModule {
}
